var jsonString = `{
  "records": [
    {
      "sitename": "豐原",
      "county": "臺中市",
      "count": "47",
      "longitude": "120.74252414",
      "latitude": "24.25699731"
    },
    {
      "sitename": "沙鹿",
      "county": "臺中市",
      "count": "94",
      "longitude": "120.568794",
      "latitude": "24.225628"
    },
    {
      "sitename": "大里",
      "county": "臺中市",
      "count": "68",
      "longitude": "120.67844444",
      "latitude": "24.09961111"
    },
    {
      "sitename": "南屯區",
      "county": "臺中市",
      "count": "98",
      "longitude": "120.6061328892508",
      "latitude": "24.147708994548818"
    },
    {
      "sitename": "忠明",
      "county": "臺中市",
      "count": "69",
      "longitude": "120.641092",
      "latitude": "24.151958"
    },
    {
      "sitename": "西屯",
      "county": "臺中市",
      "count": "78",
      "longitude": "120.616917",
      "latitude": "24.162197"
    },
    {
      "sitename": "西區",
      "county": "臺中市",
      "count": "33",
      "longitude": "120.66332185492006",
      "latitude": "24.143270346022103"
    },
        {
      "sitename": "北區",
      "county": "臺中市",
      "count": "28",
      "longitude": "120.6835863077473",
      "latitude": "24.15703734363751"
    },
         {
      "sitename": "北屯區",
      "county": "臺中市",
      "count": "56",
      "longitude": "120.6860359988246",
      "latitude": "24.181435562842605"
    },
    {
      "sitename": "南區",
      "county": "臺中市",
      "count": "34",
      "longitude": "120.66227642109048",
      "latitude": "24.121149636397824"
    },
    {
      "sitename": "太平區",
      "county": "臺中市",
      "count": "48",
      "longitude": "120.71648847465683",
      "latitude": "24.12416197016129"
    },
     {
      "sitename": "東區",
      "county": "臺中市",
      "count": "19",
      "longitude": "120.69491612897981",
      "latitude": "24.142880515030974"
    },
     {
      "sitename": "烏日區",
      "county": "臺中市",
      "count": "68",
      "longitude": "120.63714387129501",
      "latitude": "24.108396790927788"
    },
    {
      "sitename": "中區",
      "county": "臺中市",
      "count": "14",
      "longitude": "120.68151707276675",
      "latitude": "24.14038925446919"
    }
  ]
}
`